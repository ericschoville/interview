# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160908015940) do

  create_table "address_types", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "address_types", ["name"], name: "index_address_types_on_name", unique: true

  create_table "addresses", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "address_type_id"
    t.string   "line_1",          null: false
    t.string   "line_2"
    t.string   "city",            null: false
    t.string   "state",           null: false
    t.string   "postal_code",     null: false
    t.string   "country",         null: false
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "addresses", ["address_type_id"], name: "index_addresses_on_address_type_id"
  add_index "addresses", ["person_id"], name: "index_addresses_on_person_id"

  create_table "email_types", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "email_types", ["name"], name: "index_email_types_on_name", unique: true

  create_table "emails", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "email_type_id"
    t.string   "email",         null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "emails", ["email_type_id"], name: "index_emails_on_email_type_id"
  add_index "emails", ["person_id"], name: "index_emails_on_person_id"

  create_table "people", force: :cascade do |t|
    t.string   "username",     null: false
    t.string   "display_name", null: false
    t.date     "birthdate"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "people", ["display_name"], name: "index_people_on_display_name"
  add_index "people", ["username"], name: "index_people_on_username"

  create_table "phone_types", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "phone_types", ["name"], name: "index_phone_types_on_name", unique: true

  create_table "phones", force: :cascade do |t|
    t.integer  "person_id"
    t.integer  "phone_type_id"
    t.string   "phone",         null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "phones", ["person_id"], name: "index_phones_on_person_id"
  add_index "phones", ["phone_type_id"], name: "index_phones_on_phone_type_id"

end
