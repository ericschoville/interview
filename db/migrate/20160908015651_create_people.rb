class CreatePeople < ActiveRecord::Migration
  def change
    create_table :people do |t|
      t.string :username, null: false
      t.string :display_name, null: false
      t.date :birthdate

      t.timestamps null: false
    end
    add_index :people, :username
    add_index :people, :display_name
  end
end
