class CreatePhones < ActiveRecord::Migration
  def change
    create_table :phones do |t|
      t.references :person, index: true, foreign_key: true
      t.references :phone_type, index: true, foreign_key: true
      t.string :phone, null: false

      t.timestamps null: false
    end
  end
end
