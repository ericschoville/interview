class CreatePhoneTypes < ActiveRecord::Migration
  def change
    create_table :phone_types do |t|
      t.string :name, null: false

      t.timestamps null: false
    end
    add_index :phone_types, :name, unique: true
  end
end
