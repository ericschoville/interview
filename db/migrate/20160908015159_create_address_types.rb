class CreateAddressTypes < ActiveRecord::Migration
  def change
    create_table :address_types do |t|
      t.string :name, null: false

      t.timestamps null: false
    end
    add_index :address_types, :name, unique: true
  end
end
