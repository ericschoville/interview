class CreateEmailTypes < ActiveRecord::Migration
  def change
    create_table :email_types do |t|
      t.string :name, null: false

      t.timestamps null: false
    end
    add_index :email_types, :name, unique: true
  end
end
