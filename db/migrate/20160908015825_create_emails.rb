class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.references :person, index: true, foreign_key: true
      t.references :email_type, index: true, foreign_key: true
      t.string :email, null: false

      t.timestamps null: false
    end
  end
end
