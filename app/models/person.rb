class Person < ActiveRecord::Base

  validates_presence_of [:username, :display_name]
  validates_uniqueness_of [:username, :display_name]

  has_many :addresses
  has_many :phones 
  has_many :emails

end
