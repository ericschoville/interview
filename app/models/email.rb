class Email < ActiveRecord::Base
  belongs_to :person
  belongs_to :email_type
end
