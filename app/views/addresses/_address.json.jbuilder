json.extract! address, :id, :person_id, :address_type_id, :line_1, :line_2, :city, :state, :postal_code, :country, :created_at, :updated_at
json.url address_url(address, format: :json)