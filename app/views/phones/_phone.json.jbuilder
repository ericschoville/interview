json.extract! phone, :id, :person_id, :phone_type_id, :phone, :created_at, :updated_at
json.url phone_url(phone, format: :json)