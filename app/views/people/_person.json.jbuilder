json.extract! person, :id, :username, :display_name, :birthdate, :created_at, :updated_at
json.url person_url(person, format: :json)