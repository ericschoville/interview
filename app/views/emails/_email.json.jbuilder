json.extract! email, :id, :person_id, :email_type_id, :email, :created_at, :updated_at
json.url email_url(email, format: :json)