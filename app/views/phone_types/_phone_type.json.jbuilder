json.extract! phone_type, :id, :name, :created_at, :updated_at
json.url phone_type_url(phone_type, format: :json)